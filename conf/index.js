/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Default configuration for the framework
 * @module zim/conf
 * @author Eric Satterwihte 
 * @since 0.0.1
 */

exports.megadoomer = {
	applications:[],
	mail:{
		defaultemail:'noreply@mail.com'
		,defaultsender:'noreply'
		,transport:'console'
		,admins:[
			// "admin@mail.com"
		]
		,smtp:{
			host:'localhost'
			,port:25
		}
	}
	,databases:{}
}
