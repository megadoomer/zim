/*jshint node: true, laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * General database connection handler
 * @module alice-core/lib/db/connection
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires util
 * @requires events
 * @requires mongoose
 * @requires debug
 * @requires alice-conf
 * @requires alice-log
 * @requires alice-stdlib/class
 * @requires alice-stdlib/class/options
 * @requires alice-stdlib/lang
 */

var thinky          = require('thinky')
  , util            = require('util')
  , url             = require('url')
  , events          = require('events')
  , logger          = require('bitters')
  , Postgres        = require('@allstar/postgres')
  , debug           = require('debug')('megadoomer:zim:db:connection')
  , clone           = require('gaz/lang').clone
  , Class           = require('gaz/class')
  , Options         = require('gaz/class/options')
  , options         = null
  , connectors
  , Connection
  ;

const noop = () =>{}

connectors = {
  rethinkdb: {
    format: (name, options) => {

      options.db = options.db || options.pathname || name
      return options;
    }
    ,connect:function( opts ){
      var conn = new thinky( opts );

      conn.on = conn.removeAllListeners = noop;
      conn.close = () => {
        conn.r.getPool().drain()
      }

      // short cut function to create a model
      // similar to mongoose model function
      conn.model = conn.createModel;
      return conn;
    }
  }
, postgres: {
    format: ( name, options ) => {
      options.database = options.database || options.db || options.pathname || name
      return options
    }
  , connect: ( options ) => {
      const conn = new Postgres(options)
      conn.model = noop
      return conn
    }
  }
}

/**
 * Represents a connection to a datastore. May contain reference to multiple databases
 * @constructor
 * @alias module:alice-core/lib/db/connection
 * @param {Object} [options] Connection instance options
 * @param {String} [options.default=null] the name of the database to treat as the default
 * @param {Boolean} [options.autoload=true] try to reconnect if disconnected
 * @param {Object} [options.databases] An object defining the set of databases to connect to defaults to the
 */
Connection = new Class(/* @lends module:alice-core/lib/db/connection.prototype */{
  inherits:events.EventEmitter
  ,mixin: Options
  ,options:{
    default: null
    ,autoload: true
    ,databases: {}
  }
  ,constructor: function( options ){
    var str
      , fn
      , that = this
      , cache = {}
      , databases
      ;

    this.setOptions( options );

    databases = this.options.databases;
    Object
      .keys( databases )
      .forEach(function( db ){
        logger.info( 'connection to %s database', db );

        cache[ db ] = this.connect( db, databases[ db ]);
        cache[ db ].on('error', this.onError.bind( this, db ) );

        // enable the default
        if( /true/.test(databases[db].default) ){
          that.setOptions({default: db });
        }

        Object.defineProperty(this, db,{
          configurable: false
          ,enumberable: false
          ,get: function( ){
            return cache[ db ];
          }
        });

        /**
         * @name module:alice-core/lib/db/connection#db:connect
         * @event
         * @param {String} name description
         * @param {Connection} name description
         **/
        this.emit('db:connect', db, cache[db]);

      }.bind( this ) );
      Object.defineProperty(this, 'default',{
        configurable: false
        ,enumberable: false
        ,get: function( ){
          return cache[ this.options.default ];
        }
        ,set:function( name ){
          if( cache.hasOwnProperty( name ) ){
            this.setOptions({default: name });
            return cache[ this.options.default ];
          }
        }
      });
  }

  /**
   * connects to a specific
   * @method module:alice-core/lib/db/connection#disconnect
   * @param {String} name name of the database connection
   * @param {Object} options
   * @returns {database} mogodb database object
   **/
, connect: function( name, opts ){
    opts = opts || this.options.databases[ name ]
    var that = this
      , conn
      ;

    if( this.hasOwnProperty( name ) ){
      return logger.error('Connection %s already exists', name );
    }
    logger.debug( name, opts )

    /**
     * @name connection.js.Thing#event
     * @event
     * @param {TYPE} name the name of the db about to be connected to
     **/
    this.emit( 'before:db:connect', name );
    conn = this.delegate(name, opts )

    conn.on('open', function onOpen(){
      that.emit('db:connection', name, that[ name ] );
    });

    [
      'disconnected'
      , 'connected'
      , 'connecting'
      , 'disconnecting'
      ].forEach( function( state ){
        logger.debug('adding db %s event handler for %s ', name, state )
        conn.on(state, function( ){
          logger.debug('%s state: %s ', name, state);
          that.emit( util.format( '%s:%s', name, state ),name, state );
        });
    });

    return conn;
  }

  /**
   * disconnects from a specific database
   * @chainable
   * @method module:alice-core/lib/db/connection#disconnect
   * @param {String} name The name of the db to disconnect from
   * @returns {Connection} Current connection instance
   **/
, disconnect: function( name ){
    this[ name ].close();
    this[ name ].removeAllListeners();
    return this;
  }

  /**
   * Generates a valid connection string for the configured data store
   * @method module:alice-core/lib/db/connection#format
   * @param {String} name
   * @param {Object} options
   * @return {String} a connection uri string
   **/
, format: function format( name,  options ){
    var connector;
    // if it is a connection string, just return it.
    if( typeof options == 'string' ){
      var opts = url.parse( opts )
      var driver = opts.protocol

      connector = connectors[ driver ];
      if(!connector){
        var e = new Error();
        e.name = 'ImproperlyConfigured'
        e.message = 'No database driver for ' + driver;

        throw e;
      }

      return connector.format( name, opts );
    }

    connector = connectors[ options.driver ]
    return connector.format(name, options)
  }

 ,delegate: function delegate(name, opts ){
    var dbopts = this.format( name, opts );
    logger.debug("connecting to %s", dbopts );
    return connectors[opts.driver].connect( dbopts )
  }

, onError: function onError( name, error ){
    if( !this.hasOwnProperty( name ) ){
      return;
    }

    logger.error('database error [%s] %s', name, error.message );
    logger.warning('shutting down %s', name);
    this.emit('error', error );

    this
      .disconnect( name )
      .connect( name );

  }
});

module.exports = Connection;
