/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * index.js
 * @module zim/lib/db
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires keef
 * @requires gaz/lang
 * @requires zim/lib/db/connection
 */

var Connection = require( './connection' )
  , conf       = require('keef')
  , clone      = require('gaz/lang').clone
  ;

const pkgname = conf.get('pkg:name')
exports.Connection = Connection;
exports.connection = new Connection( {databases:clone( conf.get(`${pkgname}:databases`) )} );
