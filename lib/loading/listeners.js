/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * module that knows how to load fixture files in from the project tree
 * @module zim/lib/loading/listeners
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires keef
 * @requires path
 * @requires debug
 * @requires gaz/class
 * @requires zim/lib/loading/loader
 * @example var listeners = require('zim/lib/loading/listeners')
 console.log( listeners.find( ) )
 */

var conf          = require( 'keef' )                      // hive configuration loader
  , path          = require('path')
  , debug         = require( 'debug' )('hive:loading:listeners')   // debuging instance
  , Class         = require('gaz/class')                // standard Class
  , Loader        = require('gaz/fs/loader')                          // Options mixin for Class
  , ListenerLoader                                                  // Base Loader class
  , loader                                                       // Default instance of the Base loader
  ;

/**
 * Locates and loads database listeners from selected packages and modules
 * @constructor
 * @alias module:zim/lib/loading/listeners
 * @extends module:zim/lib/loading/loader
 * @param {Object} [options] Instance specific configuration options
 * @param {String} [options.searchpath=listeners] search path to look for files to load
 * @param {RegExp} [options.filepattern=/\.js|json/] a regular expression used to qualify files
 * @example var listeners = require('zim/lib/loading/listeners')
 // using the default loader
 console.log( listeners.find( ) )
 console.log( listeners.find( 'conf', 'cache' ) )
 console.log(listeners.load('hive', 'hive-cache', 'conf' ) )

// using a new loader instance
 var loader = new listeners();
 console.log( loader.find( ) )
 console.log( loader.find( 'conf', 'cache' ) )

 console.log( loader.load('hive', 'hive-cache', 'conf' ) )

 */
ListenerLoader = new Class({
  inherits: Loader
  ,options:{
    searchpath: ''
    ,filepattern: /listeners\.js$/
    ,recurse:false
  }
  ,constructor: function( options ){
    this.parent('constructor', options )
  }
  ,toName: function( app, pth ){
    return pth.replace(this.options.extensionpattern ,'').substr(pth.lastIndexOf( path.sep )+1)
  }
});

loader = new ListenerLoader();

/**
 * Locates listeer modules located in the project
 * @static
 * @param {...String} [packages] Any number of applications to load listeners from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
*/
exports.find = function find( ){
  return loader.find.apply( loader, arguments )
}

/**
 * Returns An object containing all of the listener objects listed by app name
 * @static
 * @example
var listeners = require("zim/lib/loading").listeners
listeners.load( 'hive', 'conf' )
 * @param {...String} [packages] Any number of applications to load listeners from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
 */
exports.load = function load(  ){
  return loader.load.apply( loader, arguments )
}

exports.Loader = ListenerLoader;
