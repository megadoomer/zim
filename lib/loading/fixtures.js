/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * module that knows how to load fixture files in from the project tree
 * @module zim/lib/loading/fixtures
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires keef
 * @example var fixtures = require('zim/lib/loading/fixtures')
 console.log( fixtures.find( ) )
 */

var conf          = require( 'keef' )                        // hive configuration loader
  , debug         = require( 'debug' )('megadoomer:zim:loading:fixtures')   // debuging instance
  , Class         = require( 'gaz/class' )                // standard Class
  , Loader        = require( 'gaz/fs/loader' )                          // Options mixin for Class
  , path          = require('path')
  , FIXTURE_PATH  = 'fixtures'                     // path to where fixtures should be found
  , FixtureLoader                                                  // Base Loader class
  , loader                                                         // Default instance of the Base loader
  ;

/**
 * Base implementation of fixture loader. Can locate paths and load fixture data
 * @alias module:zim/lib/loading/fixtures
 * @constructor
 * @class module:zim/loading/fixtures
 * @extends module:zim/lib/loading/loader
 * @param {Object} [options] Instance specific configuration options
 * @param {String} [options.searchpath=schemas] search path to look for files to load
 * @param {RegExp} [options.filepattern=/\.js|json/] a regular expression used to qualify files

 * @extends <CLASS>
 * @mixes <CLASS>
 * @example var fixtures = require('zim/lib/loading/fixtures')
 var loader = new fixtures.Loader();
 console.log( loader.find( ) )
 */
FixtureLoader = new Class(/* @lends module:zim/loading/fixtures.prototype  */{
	inherits: Loader
	,options:{
		searchpath: FIXTURE_PATH
		,filepattern: /\.js|json$/
	}
	,constructor: function( options ){
		this.parent('constructor', options )
	}
	,toName: function( app, pth ){
	  return pth.replace(this.options.extensionpattern ,'').substr(pth.lastIndexOf( path.sep )+1)
	}
});

loader = new FixtureLoader();

/**
 * Locates fixture files located in the project
 * @param {...String} [package] Any number of package names to load fixtures from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
*/
exports.find = function find( ){
	return loader.find.apply( loader, arguments )
}

/**
 * Returns An object containe all of the fixture objects listed by app name
 * @param {...String} [package] Any number of package names to load fixtures from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
 */
exports.load = function load(  ){
	return loader.load.apply( loader, arguments )
}

exports.Loader = FixtureLoader;
