/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Module which houses frameworks various loading capabilities
 * @module zim/lib/loading
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires zim/lib/loading/fixtures
 * @requires zim/lib/loading/models
 * @requires zim/lib/loading/loader
 */

exports.fixtures = require('./fixtures')
exports.models   = require('./models')
