/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * module that knows how to load fixture files in from the project tree. Template resolution is done on a specificity first basis, meaning, templates found in installed packages can be overridden
 * by defining a file of the same name under the same folder structure as the package you are overridding.
 * For example if you wanted to override the `password_reset.html` template of **skoodge** packages, you would create a folder structure like this:
 * ```
 * <package>/
 * ├── index.js
 * ├── package.json
 * └── templates
 *     └── skoodge
 *         └── templates
 *             └── password_reset.html
 * 
 * ```
 * @module zim/lib/loading/templates
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires keef
 * @requires path
 * @requires glob
 * @requires fs
 * @requires nunjucks
 * @requires debug
 * @requires gaz/class
 * @requires gaz/object
 * @requires gaz/lang
 * @requires gaz/array
 * @requires gaz/function
 * @requires gaz/fs/loader
 * @requires zim/lib/loading/templates/filters
 * @example var templates = require('zim/lib/loading/templates')
 console.log( templates.packages )
 */

var conf             = require( 'keef' )                         // hive configuration loader
  , path             = require( 'path')
  , glob             = require('glob')
  , fs               = require( 'fs')
  , nunjucks         = require( 'nunjucks')
  , debug            = require( 'debug' )('zim:loading:templates')   // debuging instance
  , Class            = require( 'gaz/class')                  // standard Class
  , get              = require( 'gaz/object').get
  , clone            = require( 'gaz/lang' ).clone            // standard clone function
  , toArray          = require( 'gaz/lang' ).toArray            // standard toArray function
  , compact          = require( 'gaz/array' ).compact         // standard compact function
  , flatten          = require( 'gaz/array' ).flatten         // standard flatten function
  , attempt          = require( 'gaz/function' ).attempt      // try/catch wrapper
  , values           = require( 'gaz/object' ).values
  , Loader           = require( 'gaz/fs/loader' )                          // Options mixin for Class
  , filters          = require( './filters' )
  , hivecheck        = /^hive/                                        // regex to check if a key starts with the word hive
  , Environment      = nunjucks.Environment
  , FileSystemLoader = nunjucks.FileSystemLoader
  , TemplateLoader                                                  // Base Loader class
  , loader                                                          // Default instance of the Base loader
  ;

/**
 * Locates and loads templates from selected packages and modules, also
 * @constructor
 * @alias module:zim/lib/loading/templates
 * @extends module:zim/lib/loading/loader
 * @param {Object} [options] Instance specific configuration options
 * @param {String} [options.searchpath=templates] search path to look for files to load
 * @param {RegExp} [options.filepattern=/\.html|tpl|jna/] a regular expression used to qualify files
 * @example var templates = require('zim/lib/loading/templates')
 // using the default loader
 console.log( templates.packages() )
 templates.render("sometemplate.html",{name:'hello'}, callback(err, string));
 */
TemplateLoader = new Class({
	inherits: Loader
	,options:{
		searchpath: 'templates'
		,filepattern: /\.html|tpl|jna|txt$/
		,watch:false
		,autoescape:false
		,ignore:'+(node_modules|filters)'
	}
	,constructor: function( options ){
		this.parent('constructor', options );
		this.template = new Environment( new FileSystemLoader( this.packages(), this.options ), this.options );
		for(var key in filters ){
			this.template.addFilter( key, filters[key]);
		}
	}
	,toName: function( app, pth ){
		return pth.substr(pth.lastIndexOf( path.sep )+1);
	}
	,remap: function remap(app, loaded ){
		let tmp = loaded.path.split('/templates/');
		tmp = tmp[tmp.length-1];
		return {name:tmp, template: this.template.getTemplate( tmp ) };
	}
	,flat: function flat(){
		var out = Loader.prototype.flat.call(this);
		return out.map(function( t ){return t.template ? t.template : t.path });
	}
	,packages: function packages( ){
		var apps, paths, pkg;
		pkg = conf.get('pkg:name')

		apps  = toArray( arguments.length ? arguments : conf.get(`${pkg}:applications`) );
		debug('loading app templates for', apps)
		paths = flatten( apps.map(function(app){
			return  glob.sync( path.join( path.dirname( require.resolve( app ) ), '**', `+(${this.options.searchpath})` ) ) 
		}.bind( this )));

		paths.push(path.join(process.cwd(),this.options.searchpath ));
		paths.sort(function(a, b){
			return a.length > b.length ? -1 : 1;
		})
		debug('tempates paths ', paths );
		return paths;
	}
});

loader = new TemplateLoader();

/**
 * Locates fixture files located in the project
 * @static
 * @param {...String} [packages] Any number of applications to load templates from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
*/
exports.find = function find( ){
	return loader.find.apply( loader, arguments )
};

/**
 * Loads all template objects discovered from the `find` function
 * @static
 * @param {...String} [packages] Any number of applications to load templates from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
*/
exports.load = function load(  ){
	return loader.load.apply( loader, arguments );
};

/**
 * Locates package template directories
 * @static
 * @param {...String} [packages] Any number of applications to load templates from
 * @return Object an object where keys are app names and its value is an array of template objects 
*/
exports.packages = function( ){
	return loader.packages.apply( loader, arguments );
}

/**
 * Renders a regered template with provided data values
 * @static
 * @example
var templates = require("zim/lib/loading/templates");
templates.render( "some/template.html", {data:[]}, console.log );
 * @param {String} path path to a template file relative to the templates directory
 * @param {Object} data Object to use a template variables
 * @param {Function} callback Callback function to execute with the template has completed rendering
 */
exports.render = function render(){
	return loader.template.render.apply( loader.template, arguments );
};

/**
 * Returns An object containe all of the fixture objects listed by app name
 * @static
 * @example
var templates = require("zim/lib/loading/templates")
templates.renderString( "Hello {{ name }}", {name:"Hive"}, function( err, str ){
	console.log( str );
});
 * @param {String} path path to a template file relative to the templates directory
 * @param {Object} data Object to use a template variables
 * @param {Function} callback Callback function to execute with the template has completed rendering
 */
exports.renderString = function renderString(){
	return loader.template.renderString.apply( loader.template, arguments );
};

/**
 * Returns An object containe all of the fixture objects listed by app name
 * @static
 * @param {String} name path to a template file relative to the templates directory
 * @param {Function} filter a function to transform tempate values
 * @example
var templates = require("zim/lib/loading/templates")
templates.addFilter('fake', function(){})
 */
exports.addFilter = function addFilter( ){
	return loader.template.addFilter.apply( loader.template, arguments );
};

exports.addExtension = 
exports.addTag = function addTag( ){
	return loader.template.addExtension.apply( loader.template, arguments );
};

exports.Loader = TemplateLoader;
