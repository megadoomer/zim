/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Template filter to convert integers into a USD formatted string
 * @module zim/lib/loading/templates/filters/currency
 * @author Eric satterwhite
 * @since 0.2.0
 * @requires zim/exceptions
 * @requires gaz/number
 * @requires gaz/lang
 */

var number = require('gaz/number')
  , isInteger = require('gaz/lang').isInteger
  ;

module.exports = function currency( num, kwargs ){
	if(!isInteger(num)){
		var e = new Error()
		e.name = "TemplateFilterException"
		e.message = "currency filter must be an integer - Got: " + num
		throw e;
	}
	return number.formatCurrency(num/100, kwargs )
};
