/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Template filter to convert integers into a USD formatted string
 * @module zim/lib/loading/templates/filters/pad
 * @author Eric satterwhite
 * @since 0.2.0
 * @requires zim/exceptions
 * @requires gaz/number
 * @requires gaz/lang
 */

var number = require('gaz/number')
  , isInteger = require('gaz/lang').isInteger
  ;

module.exports = function pad( num, count, character ){
	return number.pad(num, count, character );
};
