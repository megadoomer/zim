/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * module that knows how to load fixture files in from the project tree
 * @module zim/lib/loading/models
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires keef
 * @example var models = require('zim/lib/loading/models')
 console.log( models.find( ) )
 */

var conf          = require( 'keef' )                      // hive configuration loader
	, path          = require('path')
	, debug         = require( 'debug' )('hive:loading:models')   // debuging instance
	, Class         = require('gaz/class')                // standard Class
	, Loader        = require('gaz/fs/loader')                          // Options mixin for Class
	, merge         = require('gaz/object').merge
	, attempt       = require( 'gaz/function' ).attempt // try/catch wrapper
	, clone         = require('gaz/lang').clone
	, toArray       = require('gaz/lang').toArray
	, ModelLoader                                                  // Base Loader class
	, loader                                                       // Default instance of the Base loader
	, applications
	;

applications = toArray( conf.get('hive:applications') )
/**
 * Locates and loads database models from selected packages and modules
 * @constructor
 * @alias module:zim/lib/loading/models
 * @extends module:zim/lib/loading/loader
 * @param {Object} [options] Instance specific configuration options
 * @param {String} [options.searchpath=models] search path to look for files to load
 * @param {RegExp} [options.filepattern=/\.js|json/] a regular expression used to qualify files
 * @example var models = require('zim/lib/loading/models')
 // using the default loader 
 console.log( models.find( ) )
 console.log( models.find( 'conf', 'cache' ) )
 console.log(models.load('hive', 'hive-cache', 'conf' ) )

// using a new loader instance
 var loader = new models();
 console.log( loader.find( ) )
 console.log( loader.find( 'conf', 'cache' ) )

 console.log( loader.load('hive', 'hive-cache', 'conf' ) )

 */
ModelLoader = new Class(/* @lends module:zim/lib/loading/models.prototype  */{
	inherits: Loader
	,options:{
		searchpath: 'models'
		,filepattern: /\.js|json$/
	}

	,appName: function( name ){
		return path.basename( name );
	}

	,load: function load( ){
		var packages = this.find.apply(this, arguments)
		, obj = {}
		;

		Object.defineProperty(obj,'flat',{
			value:this.flat
		});

		for( var key in packages ){
			this.cache[key] = this.cache[key] || {};
			if( !this.cache[key].length ){
				this.cache[key] = merge.apply(merge, packages[key].map(this.remap.bind( this )));
			}
			obj[key] = clone( this.cache[key]);
		}
		return obj;
	}

	,remap: function remap( loaded ){
		return attempt(function(){
			var tmp = {};
			tmp[loaded.name] = require( loaded.path )
			return tmp;
		});
	}
	,toName: function( app, pth ){
		return pth.replace(this.options.extensionpattern ,'').substr(pth.lastIndexOf( path.sep )+1)
	}
});

loader = new ModelLoader();

/**
 * Locates fixture files located in the project
 * @static
 * @param {...String} [packages] Any number of applications to load models from
 * @return Object an object where keys are app names and its value is an array of full paths to all of the fixture files that were found
*/
exports.find = function find( ){
	return loader.find.apply( loader, arguments )
};

/**
 * Returns An object containe all of the fixture objects listed by app name
 * @static
 * @example
var models = require("zim/lib/loading").models
models.load( 'hive', 'conf' )
 * @param {...String} [packages] Any number of applications to load models from
 * @return Object an object where keys are app names and its value is an array the fixture data to all of the fixture files that were found
 */
exports.load = function load(  ){
	return loader.load.apply( loader, arguments )
};

exports.Loader = ModelLoader;
