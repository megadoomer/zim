/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Generates email message ids compilant with [RFC-2822](http://www.rfc-base.org/txt/rfc-2822.txt) section 3.6.4
 * @module zim/lib/mail/message/id
 * @author Eric satterwhite
 * @since 0.2.0
 * @requires date
 * @requires util
 * @requires os
 * @requires keef
 * @requires gaz/date
 * @requires gaz/random
 */

var util        = require( 'util' )
  , os          = require( 'os' )
  , date        = require( 'gaz/date' )
  , random      = require( 'gaz/random' )
  , conf        = require( 'keef' )
  , MAIL_DOMAIN = conf.get( 'mail:domain' )
  ;



/**
 * @alias module:zim/lib/mail/message/id
 * @param {String} [id]
 * @param {String} [domain] 
 */
module.exports = function( id, domain ){

	var stamp = date.format(new Date(), "%Y%m%d%H%S")
	domain = domain || MAIL_DOMAIN || os.hostname()
	return util.format(
		'<%s.%s.%d%s@%s>'
		, stamp
		, process.pid
		, random.randInt(0,100000) + process.hrtime()[1]
		, id ? ( '.' + id ) : ''
		, domain 
	);
};
