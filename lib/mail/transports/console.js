/*jshint laxcomma: true, smarttabs: true*/
'use strict';
/**
 * A terminal email transport that just writes the message to STDOUT
 * @module zim/lib/mail/transports/console
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires util
 * @requires chalk
 * @requires nodemailer
 * @requires nodemailer-stub-transport
 * @requires gaz/class
 * @requires zim/lib/mail/transports/memory
 */

var events  = require( 'events')
  , util    = require( 'util' )
  , os      = require( 'os')
  , chalk   = require( 'chalk' )
  , mailer  = require( 'nodemailer' )
  , stub    = require( 'nodemailer-stub-transport' )
  , Class   = require( 'gaz/class' )
  , Options = require( 'gaz/class/options' )
  , Parent  = require( 'gaz/class/parent' )
  , Memory  = require( './memory' )
  , Console
  ;

/**
 * @constructor
 * @alias module:zim/lib/mail/transports/console
 * @extends module:zim/lib/mail/transports/memory
 */
Console = new Class({
  inherits:Memory
  , dispatch: function dispatch( msg, message, callback ){
    this.connection.sendMail(msg, ( err, info ) => {
      process.stdout.write( chalk.yellow( info.response.toString() ) );
      process.stdout.write( os.EOL );
      process.stdout.write( '------' );
      process.stdout.write( os.EOL );
      process.stdout.write( os.EOL );
      this.emit('send', msg, err, info );
      return callback && callback( err, info )
    });
    return this;
  }
});

module.exports = Console;
