/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Houses all available email tranports backends
 * @module zim/lib/mail/transports
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires keef
 * @requires bitters
 * @requires module:zim/lib/mail/tranports/memory
 * @requires module:zim/lib/mail/tranports/console
 * @requires module:zim/lib/mail/tranports/smtp
 * @requires module:zim/lib/mail/tranports/mandrill
 */

var conf         = require( 'keef' )
  , joi          = require( 'joi' )
  , logger       = require( 'bitters' )
  , transport    = conf.get( 'megadoomer:mail:transport' )
  , default_name = typeof transport == 'object' ? transport.name : transport
  , backendSchema // validator for transport configuration.
  , results

backendSchema = joi.alternatives().try( 
	joi.string().allow('memory', 'console','smtp').default('memory') 
	,joi.object({
		name:joi.string().required().description('unique name for the transport')
		,module:joi.string().required().description('a module path that can be required')
	})
).default('memory')

// validate 
results = backendSchema.validate( transport );

if ( results.error ){
	logger.error(results.error.annotate())
	throw results.error
}

/**
 * @readonly
 * @property {Object} memory In memory email transport. Emails will be stored in an `outbox` property on the transport
 **/
exports.memory   = require( './memory')
/**
 * @readonly
 * @property {Object} console An email transport that will send all email output to stdout
 **/
exports.console  = require( './console')
/**
 * @readonly
 * @property {Object} smtp the base smtp tranpsport suitable for sending to an smtp relay server
 **/
exports.smtp     = require( './smtp')



if( typeof results.value == 'object'){
	/**
	 * @readonly
	 * @property {Object} default The transport which has been configured to be the default
	 **/
	exports.default = require( transport.module );
	default_name = transport.name;	
} else{ 
	exports.default = exports[ results.value ]
	default_name = results.value
}

if( !exports.default ){
	logger.error("unable to locate default mail transport %s", default_name )
} else{
	logger.notice("setting default mail transport: %s", default_name )
}

