/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * A terminal email transport that just writes the message to STDOUT
 * @module zim/lib/mail/transports/smtp
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires util
 * @requires nodemailer
 */

var events  = require( 'events')
  , util    = require( 'util' )
  , os      = require( 'os')
  , chalk   = require( 'chalk' )
  , mailer  = require( 'nodemailer' )
  , Class   = require( 'gaz/class' )
  , logger  = require( 'bitters' )
  , conf    = require( 'keef' )
  , smtpool = require( 'nodemailer-smtp-pool')
  , Options = require( 'gaz/class/options' )
  , Parent  = require( 'gaz/class/parent' )
  , toArray = require( 'gaz/lang' ).toArray
  , Memory  = require( './memory' )
  , SMTP
  ;

/**
 * @constructor
 * @alias module:zim/lib/mail/transports/smtp
 * @extends module:zim/lib/mail/transports/memory
 */
SMTP = new Class({
	
	inherits:Memory
	,options:{
		port: 25
	  , host: 'localhost'
	  , secure: false
	  , auth: null
	  , name: os.hostname()
	  , localAddress: '0.0.0.0'
	  , connectionTimeout: 1000 * 10
	  , debug: false
	  , authMethod: 'PLAIN'
	  , maxConnecdtions: conf.get( 'mail:connections' )
	}

	,open: function open( ){
		return mailer.createTransport( smtpool( this.options ) )
	}

	,dispatch: function dispatch(email, msg, cb ){
		this.connection.sendMail( email, function(err, info ){
			if( err ){
				return logger.error("Problem dispatching email %s", err.message, logger.exception.getAllInfo( err ) )
			}
			/**
			 * @name module:zim/lib/mail/transports/smtp#send
			 * @event
			 * @param {EmailMessage} msg message object about to be sent
			 * @param {Error|null} err an error encountered during sending, ir there was one
			 * @param {Object} info original response object returned from the transport
			 **/	
			this.emit( 'send', msg, err, info )
			logger.info( "message sent", info )
			return cb && cb( err );
		}.bind( this ))
		return this;
	}
});

module.exports = SMTP;
