/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * An in memory Mail transport that stores messages in a local outbox
 * @module zim/lib/mail/transports/memory
 * @author Eric satterwhite
 * @since 0.0.1
 * @requires util
 * @requires events
 * @requires os
 * @requires nodemailer
 * @requires nodemailer-stub-transport
 * @requires gaz/class
 * @requires gaz/class/options
 * @requires gaz/class/parent
 * @requires gaz/lang
 */

var events  = require( 'events')
  , util    = require( 'util' )
  , os      = require( 'os')
  , async   = require('async')
  , mailer  = require( 'nodemailer' )
  , stub    = require( 'nodemailer-stub-transport' )
  , Class   = require( 'gaz/class' )
  , noop    = require('gaz/function').noop
  , logger  = require( 'bitters' )
  , Options = require( 'gaz/class/options' )
  , Parent  = require( 'gaz/class/parent' )
  , toArray = require( 'gaz/lang' ).toArray
  , typeOf  = require( 'gaz/typeOf')
  , EmailMessage = require( '../message').EmailMessage
  , Memory
  ;



/**
 * @constructor
 * @alias module:zim/lib/mail/transports/memory
 * @extends module:events.EventEmitter
 * @mixes module:gaz/class/options
 * @mixes module:gaz/class/parent
 * @fires module:zim/lib/mail/transports/memory#before_send
 * @fires module:zim/lib/mail/transports/memory#send
 * @param {Object} options
 */
Memory = new Class({
	inherits:events.EventEmitter
	,mixin: [ Options, Parent ]
	,options:{

	}

	,constructor: function( options ){
		this.setOptions( options )
		this.connection = this.open( );
	}

	/**
	 * DESCRIPTION
	 * @method module:zim/lib/mail/transports/memory#open
	 * @param {TYPE} NAME
	 * @param {TYPE} NAME
	 * @return {Object}
	 **/
	, open: function open( ){
		return mailer.createTransport( stub() )
	}

	/**
	 * Sends one or more email messages
	 * @chainable
	 * @method module:zim/lib/mail/transports/memory#send
	 * @param {...EmailMessage} message The email message(s) to send off
	 * @return {module:zim/lib/mail/transports/memort} Class instance
	 **/
	, send: function send( /* message */ ){
		var args = toArray( arguments );
		var msg
		var callback;

		callback =  typeof args[ args.length - 1] == 'function' ? Array.prototype.pop.apply( args ) : noop;
		async.each( args, function( msg, cb){

			msg = typeOf( msg ) == 'email' ? msg : new ( this.message() )( msg )
			/**
			 * @name module:zim/lib/mail/transports/memory#before_send
			 * @event
			 * @param {EmailMessage} msg message object about to be sent
			 **/	
			this.emit('before_send', msg)
			this.dispatch(msg.payload, msg, cb );
		}.bind( this ), function( err ){
			callback( err )
		}.bind( this ) );
		return this;
	}

	, message: function( ){
		return EmailMessage;
	}
	/**
	 * Does the work of sending off a single message
	 * @chainable
	 * @method module:zim/lib/mail/transports/memory#dispatch
	 * @param {EmailMessage} msg
	 * @return {module:zim/lib/mail/transports/memory} the current 
	 **/
	, dispatch: function dispatch( email, message, cb ){
		this.connection.sendMail( email, function( err, info ){
			var data = {
				id:info.messageId
				,from:info.envelope.from
				,to:info.envelope.to
				,message: info.response
			};

			(this.outbox = this.outbox || []).push( data )

			/**
			 * @name module:zim/lib/mail/transports/memory#before_send
			 * @event
			 * @param {EmailMessage} msg message object about to be sent
			 * @param {Error|null} err an error encountered during sending, ir there was one
			 * @param {Object} info original response object returned from the transport
			 **/	
			this.emit( 'send', email, err, info )
			cb && cb( err )
		}.bind( this ))
		return this;
	}
});

module.exports = Memory;
