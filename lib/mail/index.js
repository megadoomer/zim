/*jshint laxcomma: true, smarttabs: true, node:true */
'use strict';
/**
 * Module for creating and sending Email messages
 * @module zim/lib/mail
 * @author Eric Satterwhite
 * @since 0.2.0
 * @requires util
 * @requires zim/lib/mail/transports
 */

var util       = require( 'util' )
  , logger     = require('bitters')
  , conf       = require( 'keef' )
  , attempt    = require('gaz/function').attempt
  , transports = require('./transports')
  , exceptions = require( '../../exceptions')
  , backends   = {}
  , Mail
  , getTransport
  ;

getTransport = function( name ){
	var bkend = backends[name] 
	if( !backends.hasOwnProperty( name ) ){
		bkend = attempt(
			function(){
				return new transports[name]( conf.get( 'mail:' + name ) || {} );
			}
		)

		if( !bkend ){
			var error = new exceptions.ImproperlyConfigured({
				message: util.format( 'unable to create mail backend %s', name )
			})
			logger.error('%s - %s' , error.name, error.message, logger.exception.getAllInfo( error ) )
			throw error;
		} else{
			backends[name] = bkend;
		}
	}
	return backends[ name ];
}

/**
 * ### Sending Email
 *
 * The mail module automatically sets up a fully configured mail transport. If the default is sufficient,
 * all you need to do is construct a message and pass it to the {@link module:zim/lib/mail#send|send} method.
 * ```javascript
	mail.send({to:'mail@mail.com', from:'noreply@spiritshop.com', text:'this is an email'}) 
 *```
 * ## Alernate Transports
 * The mail module it self is a function which you can use to use secondary transports if the default does not suit your use cases
 * ```js
	mail('console').send({to:'mail@mail.com', from:'noreply@spiritshop.com', text:'this is an email'})
 *```
 * @alias module:zim/mail
 * @param {String} transport The name of the transport to use for sending mail
 * @example
mail.send({to:'mail@mail.com', from:'noreply@spiritshop.com', text:'this is an email'}) 
 */
Mail = function Mail( name ){
	return getTransport( name )
};

if( transports.default ){
	var bkend = getTransport( 'default' )
	
	
	// populate instances of the mail backens for use
	// backends[ 'default' ] = bkend;

} else {
	var error = new exceptions.ImproperlyConfigured({
		message: util.format( 'No default email tranport configured' )
	})
	logger.error('%s - %s' , error.name, error.message, logger.exception.getAllInfo( error ) )
	throw error;
}

// populate the default transports
for( var key in transports ){

	if( key == 'default' ){
		continue;
	}

	Object.defineProperty(Mail, key, {
		get:function(){
			return getTransport( key )
		}
	});
}

Mail.Message = require('./message/base')

Object.defineProperties( Mail, {
	/**
	 * Sends orne ore more email messages
	 * @method module:zim/lib/mail#send
	 * @param {...EmailMessage} set of messages to be send through the default transport
	 * @return {TYPE} DESCRIPTION
	 **/
	send:{
		writable:false
		,value: function( ){
			var transport // The requested mail transport
			   ;

			transport = getTransport('default');
			transport.send.apply( transport, arguments );
		}
	}

	/**
	 * Allows for processing of multiple messages before sending
	 * @method module:zim/lib/mail#bulk
	 * @param {TYPE} NAME ...
	 * @param {TYPE} NAME ...
	 * @return {TYPE} DESCRIPTION
	 **/
	,bulk: {
		writable:false
		,configurable: false
		,value: function(/* messages */){

		}
	}

	,disconnect: {
		writable: false
		,value: function(){
			for( var key in transports ){
				logger.info( 'disconnectiong %s transport ', key)
				tranports[key].close()
			}
		}
	}
});

module.exports = Mail;
