/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Module for creating and sending Email messages
 * @module zim/mail
 * @author Eric Satterwhite
 * @since 0.1.0
 */

module.exports = require('./lib/mail');
