'use strict';
var assert = require('assert')
var Message = require('../lib/mail/message/base')

describe('alice-core/mail', function(){
	describe('Message', function(){
		it('should auto generate an ID', function(){
			assert.ok(new Message().id )
		});

		it('should throw if their is no subject', function(){
			assert.throws(function(){
				new Message().payload;
			});

			assert.doesNotThrow(function(){
				new Message({subject:"fake"}).payload
			})
		});

		describe('~recipients', function(){
			it('should be a single Array', function( ){
				assert.ok( Array.isArray( new Message().recipients ) )
			})
			it("should be a list of all recipient types - to, bcc, cc", function( ){
				var m = new Message({
					to:'a@mail.com'
					,cc:"b@mail.com"
					,bcc:"c@mail.com"
				});

				var r = m.recipients;
				assert.ok( r.indexOf('a@mail.com') >= 0 )
				assert.ok( r.indexOf('b@mail.com') >= 0 )
				assert.ok( r.indexOf('c@mail.com') >= 0 )
				assert.ok( r.indexOf('d@mail.com') == -1 )
			});
		})
		describe("~to", function(){
			var m1 = new Message({
				to:'e@mail.com'
			});

			var m2 = new Message({
				to:['e@mail.com']
			});

			var m3 = new Message({
				to:'a@mail.com, b@mail.com'
			});


			var m4 = new Message({});

			it('should convert strings to arrays', function(){
				assert.ok( Array.isArray( m1.to ) )
				assert.equal( m1.to[0], 'e@mail.com' )
			});

			it('should accept an array of emails', function(){
				assert.ok( Array.isArray( m2.to ) )
				assert.equal( m2.to[0], 'e@mail.com' )
			});

			it('should accept a `,` separated list of emails', function(){
				assert.ok( Array.isArray( m3.to ) )
				assert.equal( m3.to[0], 'a@mail.com' )
				assert.equal( m3.to[1], 'b@mail.com' )
			});

			it('should allow direct assigment of emails', function(){
				assert.equal( m4.to.length, 0 );
				m4.to = "a@mail.com";
				m4.to = "b@mail.com";

				assert.equal( m4.to.length, 2 );
				assert.equal( m4.to[0], 'a@mail.com' );
				assert.equal( m4.to[1], 'b@mail.com' );
			});

			it("Should only allow valid email addresses", function(){
				assert.throws(function(){
					new Message().to = "fake"
				})
			})

		});
	})
})