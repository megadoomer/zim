var assert     = require('assert')
var Mail       = require('../mail')
var MailParser = require('mailparser').MailParser
var async      = require('async')

describe('alice-core/mail', function(){
	describe('Mail', function(){
		beforeEach(function( ){
			Mail('default').outbox = null;
		});

		describe("#send", function(){
			it('it should send / collect individual messages objects', function( done ){
				Mail.send(
					{to:'a@mail.com', from:'b@mail.com', text:'hello world', 'subject':'test mail' }
					,function( err ){
						var outbox = Mail('default').outbox
						var msg = outbox[0]
						var parser = new MailParser()
						
						assert.equal(outbox.length,1 );
						parser.on('end', function( mail ){
							assert.equal(mail.text,'hello world')
							assert.equal(mail.subject, 'test mail')
							done();
						})

						parser.write( msg.message.toString('utf8'));
						parser.end();
					}
				)
			});

			it('it should send / collect individual full Message Instances', function( done ){
				Mail.send(
					new Mail.Message( {to:'a@mail.com', from:'message@mail.com', text:'hello world', 'subject':'test Message' } )
					,function( err ){
						var outbox = Mail('default').outbox
						var msg = outbox[0]
						var parser = new MailParser()
						
						assert.equal(outbox.length,1 );
						parser.on('end', function( mail ){
							assert.equal(mail.text,'hello world')
							assert.equal(mail.subject, 'test Message')
							assert.equal( mail.from[0].address, "message@mail.com")
							done();
						})

						parser.write( msg.message.toString('utf8'));
						parser.end();
					}
				)
			});

			it('it should send / collect multiple message bjects', function( done ){
				Mail.send(
					{to:'testone@mail.com', from:'senderone@mail.com', text:'I am number one', 'subject':'test1 mail' },
					{to:'testtwo@mail.com', from:'sendertwo@mail.com', text:'I am number two', 'subject':'test2 mail' }
					,function( err ){
						var outbox = Mail('default').outbox
						var msgone = outbox[0]
						var msgtwo = outbox[1]
						var parserone = new MailParser()
						var parsertwo = new MailParser()

						assert.equal(outbox.length, 2 );
						async.parallel([
							function( cb ){
								parserone.on('end', function( mail ){
									cb( null, mail )
								});
								parserone.write( msgone.message.toString('utf8') );
								parserone.end();
							}
							,function(cb ){
								parsertwo.on('end', function( mail ){
									cb( null, mail )
								});
								parsertwo.write( msgtwo.message.toString('utf8') );
								parsertwo.end();
							}
						], function( err, results ){
							var mail_1 = results[0];
							var mail_2 = results[1];

							assert.equal(mail_1.text,'I am number one');
							assert.equal(mail_1.subject, 'test1 mail');

							assert.equal(mail_2.text,'I am number two');
							assert.equal(mail_2.subject, 'test2 mail');
							done();
						});

					}
				)
			});

		});
	})
})